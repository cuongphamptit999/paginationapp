package vn.ptit.pagination;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private RecyclerView rvUser;
    private UserAdapter userAdapter;
    private List<User> mListUser;

    private boolean isLoading;
    private boolean isLastPage;
    private int totalPage = 5;
    private int currentPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvUser = findViewById(R.id.rvUser);
        LinearLayoutManager manager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rvUser.setLayoutManager(manager);

        userAdapter = new UserAdapter();
        rvUser.setAdapter(userAdapter);

        setFistData();

        rvUser.addOnScrollListener(new PaginationScrollListener(manager) {
            @Override
            public void loadMoreItem() {
                isLoading = true;

                currentPage += 1;
                loadNextPage();
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }
        });
    }

    private void loadNextPage() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                List<User> list = getListUser();

                userAdapter.removeFooterLoading();
                mListUser.addAll(list);
                userAdapter.notifyDataSetChanged();

                isLoading = false;
                if (currentPage < totalPage) {
                    userAdapter.addFooterLoading();
                } else {
                    isLastPage = true;
                }
            }
        }, 2000);

    }

    private void setFistData() {
        mListUser = getListUser();
        userAdapter.setData(mListUser);

        if (currentPage < totalPage) {
            userAdapter.addFooterLoading();
        } else {
            isLastPage = true;
        }
    }

    private List<User> getListUser() {
        Toast.makeText(this, "Load data page " + currentPage, Toast.LENGTH_SHORT).show();
        List<User> list = new ArrayList<>();
        for (int i = 1; i <= 20; i++) {
            list.add(new User("User name"));
        }
        return list;
    }
}